package com.hkhfungame;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.Constant;
import com.hkhfungame.databinding.ActivityNotificationBinding;
import com.hkhfungame.databinding.ActivityNotificationBinding;
import com.hkhfungame.databinding.CustomerRowBinding;
import com.hkhfungame.databinding.NotificationRowBinding;

import org.json.JSONArray;

public class Notification extends AppCompatActivity {
    ActivityNotificationBinding binding;
    JSONArray customer_list =new JSONArray();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityNotificationBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());


        Toolbar toolbar = findViewById(R.id.toolbarr);
        Constant.setToolbar(Notification.this, toolbar);

        RecyclerAdapter1 recyclerAdapter = new RecyclerAdapter1(Notification.this, customer_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(Notification.this);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        binding.recycler.setLayoutManager(layoutManager);
        binding.recycler.setAdapter(recyclerAdapter);
    }

    public class RecyclerAdapter1 extends RecyclerView.Adapter<RecyclerAdapter1.RecyclerViewHolder> {
        NotificationRowBinding binding;
        Context context;
        JSONArray listItem;

        public RecyclerAdapter1(Context context, JSONArray listItem) {
            this.context = context;
            this.listItem = listItem;
        }


        @NonNull
        @Override
        public RecyclerAdapter1.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = NotificationRowBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            RecyclerAdapter1.RecyclerViewHolder holder = new RecyclerAdapter1.RecyclerViewHolder(binding);

            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerAdapter1.RecyclerViewHolder holder, int position) {


            holder.setIsRecyclable(false);
        }

        @Override
        public int getItemCount() {
            return 10;
        }


        public class RecyclerViewHolder extends RecyclerView.ViewHolder {
            public RecyclerViewHolder(@NonNull NotificationRowBinding itemView) {
                super(itemView.getRoot());
            }
        }
    }
}