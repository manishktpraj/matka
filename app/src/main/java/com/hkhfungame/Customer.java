package com.hkhfungame;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.Constant;
import com.hkhfungame.databinding.ActivityCustomerBinding;
import com.hkhfungame.databinding.CustomerRowBinding;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Customer extends AppCompatActivity {
    ActivityCustomerBinding binding;
    JSONArray customer_list =new JSONArray();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityCustomerBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());


        Toolbar toolbar = findViewById(R.id.toolbarr);
         Constant.setToolbar(Customer.this, toolbar);

        RecyclerAdapter1 recyclerAdapter = new RecyclerAdapter1(Customer.this, customer_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(Customer.this);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        binding.recycler.setLayoutManager(layoutManager);
        binding.recycler.setAdapter(recyclerAdapter);
    }

    public class RecyclerAdapter1 extends RecyclerView.Adapter<RecyclerAdapter1.RecyclerViewHolder> {
        CustomerRowBinding binding;
        Context context;
        JSONArray listItem;

        public RecyclerAdapter1(Context context, JSONArray listItem) {
            this.context = context;
            this.listItem = listItem;
        }


        @NonNull
        @Override
        public RecyclerAdapter1.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = CustomerRowBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            RecyclerAdapter1.RecyclerViewHolder holder = new RecyclerAdapter1.RecyclerViewHolder(binding);

            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerAdapter1.RecyclerViewHolder holder, int position) {
            if(position==0)
            {
                binding.topspace.setVisibility(View.VISIBLE);
            }else{
                binding.topspace.setVisibility(View.GONE);
            }

            holder.setIsRecyclable(false);
        }

        @Override
        public int getItemCount() {
            return 10;
        }


        public class RecyclerViewHolder extends RecyclerView.ViewHolder {
            public RecyclerViewHolder(@NonNull CustomerRowBinding itemView) {
                super(itemView.getRoot());
            }
        }
    }

}