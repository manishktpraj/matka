package com.hkhfungame;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 2000;
    AppCompatActivity activity;
    private SplashActivity activity1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /////FirebaseApp.initializeApp(getApplicationContext());
        setContentView(R.layout.activity_splash);
        activity = activity1;
        ///   fullScreen();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                                  Intent i = null;
                                i = new Intent(getApplicationContext(), LoginActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                                finish();

            }
        }, SPLASH_TIME_OUT);


    }


}
